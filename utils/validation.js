/**
 * validate Todo object
 * Todo Object looks like:
 * {
 *  id: string,
 *  title: string
 * }
 * - title min length should be 5
 * - should have id
 * - id and title should be string
 *
 * @param {Object} todo
 */
export function validateTodoEntry(todo) {}
