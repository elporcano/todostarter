### Week 2

#### Objectives

- Learn concepts of React
- Learn and apply knowledge of React Native
- Learn concepts of navigation for React Native using React Navigation

#### Instructions

- Sprint plan with mentor at start of week
- Implement the view side of your todo
- Together with mentor decide how you'd like to make your Todo app look
- Setup your react navigation
- use React's basic state and props management
