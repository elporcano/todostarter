### Week 1

#### Objectives

- Learn basics of Git
- Learn usage of Jira
- Learn/Review JavaScript
- Learn a bit of JavaScript unit testing
- Become comfortable with JavaScript in solving tasks

#### Instructions

- Please add all tasks below to JIRA (Mentor will provide JIRA Access and assist)
- Create a set of functions and implementations that would handle:
  - creating new todo
  - deleting todo
  - editing todo
  - listing todos
  - getting todo by specific id
- Finish function validateTodoEntry in `utils/validation.js` which can be used to validateTodo when creating and updating
- (Optional) Add `jest` unit tests for all functions when possible see example `utils/validation.spec.js`
