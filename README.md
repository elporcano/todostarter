## Todo App - Starter

This source code will serve as a starting point for the onboarding project new hires will be using to assist in learning technology needed within the first month.

Instructions and assistance will be provided by the assigned mentor.

## GOALS

https://docs.google.com/spreadsheets/d/11eS1cDMcT2XQo_jAzNaMbn_Sc9tHL6Pkx-w-mrqABgA/edit#gid=0

### Prerequisites

- Install softwares needed (mentor to help)
- Jira account added to onboarding project
- Bitbucket account

### Instructions

- Fork and clone this repo `git@bitbucket.org:vananaz-systems/todostarter.git`
- Changes must be made on your own forked repo
- for every feature you make it should be made into different branch and made as pull request for mentor/s to review

### 1 Month progression tasks

- [Week1](./docs/week1.md)
- [Week2](./docs/week2.md)
- [Week3](./docs/week3.md)
- [Week4](./docs/week4.md)
